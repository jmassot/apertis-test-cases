metadata:
  name: connman-services-switchover
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Tests Network connection management: ensuring that ConnMan can
                connect to actual networks even after the device under test is rebooted"
  macro_install_packages_preconditions: wget
  pre-conditions:
    - "The device must have wifi, otherwise skip this test."
    - "A mobile Hot-Spot must be setup"
    - "Please note that connman disables wifi by default on a fresh image. To
       enable:"
    - $ connmanctl enable wifi
    - "If it's already enabled, connmanctl will give the error
       \"Error wifi: Already enabled\" that may be ignored."

  expected:
    - "If success \"Connected <service ID>\" will be printed by connmanctl,
       otherwise \"Agent ReportError <service ID>\" will be printed."
    - "On executing the wget command the connection should succeed and you should be able to ping any website"
    - "On switching over from the mobile hot-spot to Wi-Fi network connman should not ask the user to re-enter the password for the Wi-Fi network"

  notes:
    - "Warning: Connman changes are persistent (over reboot!). After finishing
       testing, it might be wise to perform the dhcp test to ensure that the
       network is in a sensible state."
    - "For ALL tests the enable step will give an \"In progress\" error if the
       device is already enabled. This is to be expected."
    - "When testing the SDK image in VirtualBox with an Asus USB-N10 Wireless
       adapter, firmware-realtek needs to be installed on the host."

run:
  steps:
    - "Run the ConnMan CLI Interface:"
    - $ connmanctl
    - "When run without arguments, connmanctl will launch in interactive mode
       with a \"connmanctl>\" prompt."
    - "Scan wifi:"
    - $connmanctl> scan wifi
    - "Wait for \"Scan completed for wifi\" before continuing (this may take a few
       seconds and will appear above connmanctl prompt)."
    - "Turn on the agent, to manage additional information if required:"
    - $connmanctl> agent on
    - "connmanctl should return \"Agent registered\"."
    - "List services available:"
    - $connmanctl> services
    - "Select required service and use the \"service ID\" (second column) to
       connect:"
    - $connmanctl> connect <service ID>
    - "If it's connecting to a Wi-Fi service that needs additional information
       connmanctl will output an \"Agent RequestInput (…)\" line followed by a
       prompt for the required information, such as \"Passphrase? \"."
    - "Ensure ConnMan returns \"Connected <service ID>\" before continuing."
    - "Quit from connmanctl interactive mode with:"
    - $connmanctl> quit
    - "On the terminal enter $wget www.google.com"
    - "Wget is successfull and the connection is establised via Wi-Fi"
    - "Disconnect from the Wi-Fi network"
    - $ connmanctl 
    - $connmanctl> scan wifi
    - "Connect to the mobile hot-spot that you have setup on your phone by entering the right password"
    - "Quit from connmanctl interactive mode with:"
    - $connmanctl> quit
    - $ connmanctl
    - $connmanctl> scan wifi
    - $ "Connect back to the Wi-Fi network"
    
