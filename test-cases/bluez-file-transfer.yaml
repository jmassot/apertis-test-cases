metadata:
  name: bluez-file-transfer
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Test all BlueZ features needing interaction with a phone like file transfer of images or audio/video files"

  resources:
    - "A Bluetooth adapter (if you have two plugged, please remove one to avoid
       confusion)"
    - "A Bluetooth-enabled phone with a SIM card and a data plan. (https://www.apertis.org/reference_hardware/extras/)"

  macro_ostree_preconditions:
    reponame: bluez-phone
    branch: apertis/v2023dev2

  pre-conditions:
    - "If running the test on an SDK image, kill the blueman-applet process, as it prevents the test from installing its own pairing agent."

  expected:
    - "If PASSED is displayed, all of the test have passed. The output should be
       similar to that:"
    - |
        >select_adapter
        Selected /org/bluez/hci0
        select_device: Discovering...
        Device found: F0:39:42:86:2E:1B Charge 2
        Device found: 88:83:22:2F:56:2A Galaxy A5 2016
        Input device address: 88:83:22:2F:56:2A
        Selected address: 88:83:22:2F:56:2A
        start_agent
        test_pairing_initiator
        Scanning in progress...
        Pairing request to 88:83:22:2F:56:2A in progress...
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired
        test_pairing_responder
        Start a pairing from the phone! was it successful (y/n):
        y
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired
        stop_agent
        test_profiles
        OBEXObjectPush
        test_profile_opp_server
        ./bluez-phone-file-transfer.sh: 360: kill: Usage: kill [-s sigspec | -signum | -sigspec] [pid | job]... or
        kill -l [exitstatus]
        Start an OPP transfer in the phone, was it successful? (y/n)
        y
run:
  steps:
    - "The phone must be in discoverable mode for starting this test. Look for
       the phone in this list, and save the BT address. It will be used while
       running the test."
    - "Execute the test suite inside an environment with dbus:"
    - $ ./bluez-phone-file-transfer.sh
    - "There are some options:"
    - |
        >-a select which device you want to pair with (specify the address)
        -s skip pairing because the device is already paired. -a must be present when using this.
        -x enables test debugging (only useful if a test fails)
    - "Once the test begins, after Apertis finishes pairing with the phone, you
       must initiate pairing from the phone and do the pairing again as part of
       the test. You may need to unpair Apertis from within the phone first. The
       test will display the following message when that is required:"
    - |
        >Start a pairing from the phone! was it successful (y/n).
    - "After you've initiated the pairing from the phone, the test will continue."
    - "If the pairing fails from the test, try to do the pairing separately, and
       then run the test using './bluez-phone-file-transfer.sh -a <phone addr> -s'. To do a
       separate pairing: unpair the phone, and run 'bluetoothctl'. Then, in
       bluetoothctl prompt, issue the following commands in order: 'remove
       <phone addr>', 'discoverable on', 'agent off', 'agent NoInputNoOutput',
       'default-agent'. The either 'pairable on' to pair from phone, or 'pair
       <phone addr>' to issue a pairing to the phone. '<phone addr>' is the phone
       address of the form 'a1:b2:c3:d4:e5:f6'."
    - "The next step is to initiate a file transfer from the phone. This can be
       done by sending a contact from the phone. The test will display the
       following message when that is required:"
    - |
        >Start an OPP file transfer from the phone to the target, was it successful? (y/n)
    - "After you’ve initiated the file transfer from the phone, the test will continue."
    - "Next the device will send a file to the phone"
    - "Accept the incoming file transfer"
